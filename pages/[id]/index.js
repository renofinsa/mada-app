import Head from 'next/head'
import styles from '../../styles/Home.module.css'
import { db } from '../../services/adaptor'
import { collection, getDocs, query, where } from 'firebase/firestore';
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';

export default function Detail() {

  const router = useRouter();

  const [state, setState] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if (router.query.id) {
      getData();
    }
  }, [router]);

  const getData = async () => {
    setLoading(true);
    let groupId = router.query.id.split('-')
    let getCol = query(collection(db, 'DATA'), where('groupId','==',parseInt(groupId[1])))
    let getSnapshot = await getDocs(getCol);
    
    let payload = []
    getSnapshot.docs.forEach(item => {
      payload.push(Object.assign(item.data(), {id: item.id}))
    })
    
    setState(payload);
    setLoading(false);
  }

  if (loading) {
    return <h1>Loading</h1>
  }

  return (
    <div className={styles.container}>
      <Head>
        <title>Mada App - {state[0].title}</title>
        <meta name="title" content={`${state[0].title} - ${state[0].subtitle}`} />
        <meta name="description" content={state[0].description} />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div>
        <div>
          <Link href={`/`}><a>Back</a></Link>
        </div>
        {state.map((item, index) => (
          <div key={index}>
            <h4>{item.title}</h4>
            <h5>{item.subtitle}</h5>
            <p>{item.description}</p>
            <div>
              <p>
                <strong>{item.type == 'SL' ? 'Khasiat' : 'Benefit'}: </strong><br />
                {item.benefit}
              </p>
            </div>
            Sumber Data: {item.type}
          </div>
        ))}
      </div>

      {/* QUESTIONNAIRE */}
      <div>
        <h4>Questionnaire:</h4>
        <ol>
          <li>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime ipsa necessitatibus corporis. Adipisci quam velit sit odio fugit temporibus dolorem obcaecati veritatis saepe, sequi soluta! Qui exercitationem sequi natus tempora!</p>
            <textarea placeholder='Write your answer here...' rows={5} cols={35}></textarea>
          </li>
          <li>
            <p>Maxime ipsa necessitatibus corporis. Adipisci quam velit sit odio fugit!</p>
            <div>
              <input name='radio' id='radio1' type='radio' /> <label for='radio1'>Correct</label>
            </div>
            <div>
              <input name='radio' id='radio2' type='radio' /> <label for='radio2'>Incorrect</label>
            </div>
          </li>
          <li>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. !</p>
            <div>
              <input name='radio' id='radio1' type='radio' /> <label for='radio1'>Correct</label>
            </div>
            <div>
              <input name='radio' id='radio2' type='radio' /> <label for='radio2'>Incorrect</label>
            </div>
          </li>
          <li>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero earum voluptas amet minima, ab, qui est soluta consequatur quibusdam delectus numquam quasi, quo ea facere?</p>
            <textarea placeholder='Write your answer here...' rows={5} cols={35}></textarea>
          </li>
        </ol>
        <button>Submit</button>
      </div>
    </div>
  )
}
