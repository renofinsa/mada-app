import { initializeApp } from "firebase/app";
import { getFirestore } from 'firebase/firestore';

const firebaseConfig = {
  apiKey: "AIzaSyDAQEKZdsHr3ToEjRCVpOTMcdMOvYtDnb8",
  authDomain: "mada-app-b593d.firebaseapp.com",
  projectId: "mada-app-b593d",
  storageBucket: "mada-app-b593d.appspot.com",
  messagingSenderId: "5689948311",
  appId: "1:5689948311:web:9d1962158ce6a0654c51d2",
  measurementId: "G-XX705029GF"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

export { db }